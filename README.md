### Проект для хранения logic кода sentiment-analyser

#### В данном проекте реализовано:
- Хранение кода webapp приложения
- Сборка образа docker
- Пуш образа в docker hub

#### Сборка контейнера

```
$ docker build -f Dockerfile -t $DOCKER_USER_ID/sentiment-analysis-logic .
```

#### Запуск контейнера

```
$ docker run -d -p 5050:5000 $DOCKER_USER_ID/sentiment-analysis-logic
```

#### Публикация контейнера

```
$ docker push $DOCKER_USER_ID/sentiment-analysis-logic
```